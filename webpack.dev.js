const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = merge(common, {
	mode: 'development',
	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, 'dist'),
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: './src/index.html',
		}),
	],
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: [
					'style-loader', // 3rd -> injects css into dom
					'css-loader', // 2nd -> convert css in common js you can see that in main.js
					'sass-loader', // 1st -> convert scss into css
				],
			},
		],
	},
});
