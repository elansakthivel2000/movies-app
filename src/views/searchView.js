import { elements as DOM } from './base';

export const clearResults = (query) => {
	switch (query) {
		case 'movies-coming':
			DOM.comingSoonResultList.innerHTML = '';
			break;
		case 'movies-in-theatres':
			DOM.moviesInTheatreResultList.innerHTML = '';
			break;
		case 'top-rated-india':
			DOM.topRatedIndianResultList.innerHTML = '';
			break;
		case 'top-rated-movies':
			DOM.topRatedMoviesResultList.innerHTML = '';
			break;
		case 'favourit':
			DOM.favouriteMoviesResultList.innerHTML = '';
		default:
			break;
	}
};

const renderMovie = (movie, parent, query) => {
	const markup = `
        <div class="col-md-6 card mt-2">
            <div class="row">
                <div class="col-5">
                    <img class="card-img-top" src=${movie.posterurl} alt=${movie.title} />
                </div>
                <div class="col-7">
                    <div class="card-body">
                        <h5 class="card-title">Title: ${movie.title}</h5>
                        <p class="card-text">Release Date: ${movie.releaseDate}</p>
                        <p class="card-text">IMDB Rating: ${
													movie.imdbRating.length === 0 ? 'NIL' : movie.imdbRating
												}</p>
                        <a href='#${query}_${movie.id}_${movie.title}_${
		movie.releaseDate
	}' class="btn btn-primary">View More Details</a>
                    </div>
                </div>
            </div>
            
        </div>
    `;
	parent.insertAdjacentHTML('beforeend', markup);
};

export const renderResults = (movies, parent, query) => {
	movies.forEach((movie) => {
		if (query === 'favourit') {
			renderMovie(movie.movie, parent, query);
		} else {
			renderMovie(movie, parent, query);
		}
	});
};
