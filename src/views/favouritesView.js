export const toggleFavourite = (isFavourited) => {
	const el = document.querySelector('.favourite__button');
	if (isFavourited) {
		el.classList.remove('btn-success');
		el.classList.add('btn-danger');
		el.textContent = 'Remove From Favourites';
	} else {
		el.classList.remove('btn-danger');
		el.classList.add('btn-success');
		el.textContent = 'Add To Favourites';
	}
};
