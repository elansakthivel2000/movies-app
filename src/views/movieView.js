import { elements as DOM } from './base';

export const renderMovie = (query, movie, isFavourited) => {
	if (query === 'favourit') {
		movie = movie.movie;
	}
	const markup = `
        <div class="card d-flex justify-content-center" style="width: 25rem">
            <img class="card-img-top" src=${movie.posterurl} alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">${
									movie.title.length !== 0 ? movie.title : 'NIL'
								}</h5>
                <p class="card-text">${
									movie.storyline !== 0 ? movie.storyline : 'NIL'
								}</p>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Release Date: ${
									movie.releaseDate.length !== 0 ? movie.releaseDate : 'NIL'
								}</li>
                <li class="list-group-item">Content Rating: ${
									movie.contentRating.length !== 0 ? movie.contentRating : 'NIL'
								}</li>
                <li class="list-group-item">Duration: ${
									movie.duration.length !== 0 ? movie.duration : 'NIL'
								}</li>
                <li class="list-group-item">Average Rating: ${
									movie.averageRating.length !== 0 ? movie.averageRating : 'NIL'
								}</li>
                <li class="list-group-item">Genres: ${
									movie.genres.length !== 0 ? movie.genres.join(', ') : 'NIL'
								}</li>
                <li class="list-group-item">Actors: ${
									movie.actors.length !== 0 ? movie.actors.join(', ') : 'NIL'
								}</li>
            </ul>
            <div class="card-body">
                <a class='favourite__button btn text-light ${
									isFavourited ? 'btn-danger' : 'btn-success'
								}'>${!isFavourited ? 'Add To Favourite' : 'Remove From Favourite'}</a>
            </div>
        </div>
    `;
	DOM.movies.insertAdjacentHTML('afterbegin', markup);
};
