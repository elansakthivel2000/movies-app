export const elements = {
	comingSoon: document.querySelector('.coming__soon'),
	moviesInTheatres: document.querySelector('.movies__theatres'),
	topRatedIndian: document.querySelector('.toprated__indian'),
	topRatedMovies: document.querySelector('.movies__toprated'),
	favourites: document.querySelector('.favourites'),
	comingSoonResultContainer: document.querySelector('.coming-soon'),
	comingSoonResultList: document.querySelector('.coming-soon__resultList'),
	moviesInTheatreResultContainer: document.querySelector('.movies-in-theatre'),
	moviesInTheatreResultList: document.querySelector('.movies-in-theatre__resultList'),
	topRatedIndianResultContainer: document.querySelector('.top-rated-indian'),
	topRatedIndianResultList: document.querySelector('.top-rated-indian__resultList'),
	topRatedMoviesResultContainer: document.querySelector('.top-rated-movies'),
	topRatedMoviesResultList: document.querySelector('.top-rated-movies__resultList'),
	favouriteMoviesResultContainer: document.querySelector('.favourite-movies'),
	favouriteMoviesResultList: document.querySelector('.favourite-movies__resultList'),
	movies: document.querySelector('.movies'),
};

export const renderLoader = (parentElement) => {
	const loader = `
        <div class="d-flex justify-content-center loader">
            <div class="spinner-border text-primary" role="status">
                <span class="sr-only"></span>
            </div>
        </div>
    `;

	parentElement.insertAdjacentHTML('afterbegin', loader);
};

export const removeLoader = () => {
	const loader = document.querySelector('.loader');
	if (loader) {
		loader.parentElement.removeChild(loader);
	}
};

// type -> 1. success   2. danger
export const showAlert = (type, message) => {
	const alert = `
        <div class='d-flex justify-content-center'>
            <div class='alert alert-${type} alert-dismissable fade show' style="width:40%" role='alert'>
                ${message}
                <button type='button' class='close' data-dismiss='alert' aria-label='close'>
                    <span aria-hidden='true'>&times;</span>
                </button>
            </div>
        </div>
    `;
	document.querySelector('.container-fluid').insertAdjacentHTML('afterbegin', alert);
};
