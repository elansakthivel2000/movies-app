import 'axios';
import axios from 'axios';

export default class Search {
	constructor(query) {
		this.query = query;
	}

	async getResults() {
		try {
			const response = await axios.get(`http://localhost:3000/${this.query}`);
			this.results = response.data;
		} catch (error) {
			console.log(error);
		}
	}
}
