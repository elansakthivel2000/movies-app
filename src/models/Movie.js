import axios from 'axios';

export default class Movies {
	constructor(query, id, title, releaseDate) {
		this.query = query;
		this.id = id;
		this.title = title;
		this.releaseDate = releaseDate;
	}

	async getDetails() {
		try {
			let searchQuery;
			if (this.id === 'undefined') {
				searchQuery = `${this.query}?title=${this.title}&releaseDate=${this.releaseDate}`;
				const response = await axios(`http://localhost:3000/${searchQuery}`);
				this.movie = response.data[0];
			} else {
				searchQuery = `${this.query}/${this.id}`;
				const response = await axios(`http://localhost:3000/${searchQuery}`);
				this.movie = response.data;
			}
		} catch (error) {
			console.log(error);
		}
	}
}
