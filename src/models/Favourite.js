import axios from 'axios';

export default class Favourite {
	constructor() {
		this.favourites = [];
	}

	async getFavourites() {
		try {
			const response = await axios.get('http://localhost:3000/favourit');
			this.favourites = response.data;
		} catch (error) {
			console.log(error);
		}
	}

	async addFavourite(movie) {
		try {
			const response = await axios.post('http://localhost:3000/favourit', movie);
			this.favourites.push(movie);
		} catch (error) {
			console.log(error);
		}
	}

	async deleteFavourite(movie) {
		const { id, title, releaseDate } = movie.movie;
		try {
			await axios.delete(`http://localhost:3000/favourit/${id}`);
			let newFavourites;
			if (id === 'undefined') {
				newFavourites = this.favourites.filter(
					(favourite) =>
						favourite.title !== title || favourite.releaseDate !== releaseDate
				);
			} else {
				newFavourites = this.favourites.filter((favourite) => favourite.id !== id);
			}
			this.favourites = newFavourites;
		} catch (error) {
			console.log(error);
		}
	}

	isFavourited(id, title, releaseDate) {
		if (id === 'undefined') {
			return (
				this.favourites.findIndex(
					(movie) => movie.title === title && movie.releaseDate === releaseDate
				) !== -1
			);
		} else {
			return this.favourites.findIndex((movie) => movie.id === id) !== -1;
		}
	}
}
