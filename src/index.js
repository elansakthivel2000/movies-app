import Search from './models/Search';
import './vendor';

import { elements as DOM, renderLoader, removeLoader, showAlert } from './views/base';
import * as searchView from './views/searchView';
import * as movieView from './views/movieView';
import * as favouritesView from './views/favouritesView';

import './main.scss';
import Movie from './models/Movie';
import Favourite from './models/Favourite';

const state = {};

// SEARCH CONTROLLER
const searchController = async (query) => {
	// 1. Create a new search object and add to state.
	state.search = new Search(query);

	// 2. Prepare the UI for results
	searchView.clearResults(query);
	let parent;
	let resultListParent;
	switch (query) {
		case 'movies-coming':
			parent = DOM.comingSoonResultContainer;
			resultListParent = DOM.comingSoonResultList;
			break;
		case 'movies-in-theaters':
			parent = DOM.moviesInTheatreResultContainer;
			resultListParent = DOM.moviesInTheatreResultList;
			break;
		case 'top-rated-india':
			parent = DOM.topRatedIndianResultContainer;
			resultListParent = DOM.topRatedIndianResultList;
			break;
		case 'top-rated-movies':
			parent = DOM.topRatedMoviesResultContainer;
			resultListParent = DOM.topRatedMoviesResultList;
			break;
		case 'favourit':
			parent = DOM.favouriteMoviesResultContainer;
			resultListParent = DOM.favouriteMoviesResultList;
			break;
		default:
			break;
	}
	renderLoader(parent);

	// 3. Search for movies
	await state.search.getResults();

	// 4. Update the UI
	removeLoader();

	// 5. render the results
	searchView.renderResults(state.search.results, resultListParent, query);
};

searchController('movies-coming');

DOM.comingSoon.addEventListener('click', (event) => {
	searchController('movies-coming');
});

DOM.moviesInTheatres.addEventListener('click', (event) => {
	searchController('movies-in-theaters');
});

DOM.topRatedIndian.addEventListener('click', (event) => {
	searchController('top-rated-india');
});

DOM.topRatedMovies.addEventListener('click', (event) => {
	searchController('top-rated-movies');
});

DOM.favourites.addEventListener('click', (event) => {
	searchController('favourit');
});

// MOVIES CONTROLLER
const moviesController = async () => {
	const queryId = window.location.hash.replace('#', '');
	const [query, id, title, releaseDate] = queryId.split('_');

	if (id || title) {
		// 1. Prepare the UI
		DOM.movies.innerHTML = '';
		renderLoader(DOM.movies);

		state.movie = new Movie(query, id, title, releaseDate);

		try {
			await state.movie.getDetails();

			removeLoader();
			movieView.renderMovie(
				query,
				state.movie.movie,
				state.favourites.isFavourited(id, title, releaseDate)
			);
		} catch (error) {
			console.log(error);
		}
	}
};

['load', 'hashchange'].forEach((event) => {
	window.addEventListener(event, moviesController);
});

// Favourites Controller
const favouriteController = async () => {
	const currentId = state.movie.id;
	const currentTitle = state.movie.title;
	const currentReleaseDate = state.movie.releaseDate;

	if (!state.favourites) {
		state.favourites = new Favourite();
	}

	if (!state.favourites.isFavourited(currentId, currentTitle, currentReleaseDate)) {
		await state.favourites.addFavourite(state.movie);
		showAlert('success', 'Added to Favourites');
		favouritesView.toggleFavourite(true);
		searchController('favourit');
	} else {
		await state.favourites.deleteFavourite(state.movie);
		showAlert('danger', 'Removed from Favourites');
		favouritesView.toggleFavourite(false);
		searchController('favourit');
		console.log(document.querySelector('#pills-favourites-tab'));
		if (document.querySelector('#pills-favourites-tab').classList.contains('active')) {
			DOM.movies.innerHTML = '';
		}
	}
};

// Restore the favourited items when the page loads
window.addEventListener('load', () => {
	// create a new likes object
	state.favourites = new Favourite();

	// get the favourites from json.
	state.favourites.getFavourites();
});

DOM.movies.addEventListener('click', (event) => {
	if (event.target.matches('.favourite__button, .favourite__button *')) {
		favouriteController();
	}
});
