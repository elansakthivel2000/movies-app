# Movies App

## Steps to run:-

### In Development mode:-

    1. Clone the repo.
    2. Run npm install from root directory.
    3. Start the json-server first (command:- json-server src/data/data.json).
    4. Run npm start

## In Production mode:-

    1. Start the json-server first (command:- json-server src/data/data.json).
    2. Production build is available in the dist folder you can simply open index.html in the browser.
